# Task 5: Authority Finding in Twitter

In this task, the systems are challenged to retrieve a set of authority Twitter accounts for a given rumor propagating in Twitter. Given a tweet stating a rumor, a model has to retrieve a ranked list of authority Twitter accounts that can help verify the rumor; i.e. they may tweet evidence that supports or denies the rumor. This task is offered in Arabic.

## Data-sharing Agreement


**Data is available at**



__Table of contents:__
- [Evaluation Results](#evaluation-results)
- [List of Versions](#list-of-versions)
- [Contents of the Task 5 Directory](#contents-of-the-repository)
- [Input Data Format](#input-data-format)
<!-- 	- [Task 5: Multi-Class Fake News Detection of News Articles](#Subtask-3A-Multi-Class-Fake-News-Detection-of-News-Articles)
- [Output Data Format](#output-data-format)
	- [Task 5: Multi-Class Fake News Detection of News Articles](#Multi-Class-Fake-News-Detection-of-News-Articles)
- [Format Checkers](#format-checkers)
	- [Task 5: Multi-Class Fake News Detection of News Articles](#Subtask-3A-Multi-Class-Fake-News-Detection-of-News-Articles-2)
- [Scorers](#scorers)
	- [Task 5: Multi-Class Fake News Detection of News Articles](#Subtask-3A-Multi-Class-Fake-News-Detection-of-News-Articles-3)
- [Evaluation Metrics](#evaluation-metrics)
- [Baselines](#baselines)
	- [Task 3: Multi-Class Fake News Detection of News Articles](#Subtask-3A-Multi-Class-Fake-News-Detection-of-News-Articles-4)
- [Credits](#credits)
 -->
## Evaluation Results

TBA

## List of Versions

<!-- - **Task 5--Arabic-v1.0 [2022/24/03]** - data for task 5 is released. -->

## Contents of the Task 5 Directory
We provide the following files:

<!-- - Main folder: [data](./data)
  - subfolder: Task 3--english
- Main folder: [baseline](./baseline)<br/>
- 	Contains scripts provided for baseline models of the tasks
- Main folder: [evaluation](./format_checker)<br/>
- 	Contains scripts provided to evaluate submission file
- [README.md](./README.md) <br/>
- 	This file!
 -->


# Input Data Format

<!-- The data will be provided in the format of Id, title, text, rating, domain the description of column are as follows:

## Task 3
```
- public_id- Unique indetifier of the news article
- title- Title of the news article
- text- Text mentioned inside the news article
- our rating - class of the news article as false, partially false, true, other
```

# Multi-Class Fake News Detection of News Articles

Multi-class fake news detection of news articles (English): Sub-task A would be the detection of fake news designed as a four-class classification problem. The training data will be released in batches and will be roughly about 900 articles with the respective label. Given the text of a news article, determine whether the main claim made in the article is true, partially true, false, or other.

### Cross-Lingual Task

Along with the multi-class task for the English language, we have introduced a task for low resourced language. We will provide the data for dev and test in the German language. The idea of the task is to use the English data and the concept of transfer to build a classification model for the German language.

# Output Data Format

### Task 3: Multi-Class Fake News Detection of News Articles

We need the output file in the format of public_id, predicted_rating.


# Format Checkers

#### Task 3: Multi-Class Fake News Detection of News Articles

Task 3

public_id- Unique identifier of the news article
predicted_rating- predicted class
Sample File

```
public_id, predicted_rating
1, false
2, true
```

### Multi-Class Fake News Detection of News Articles -->

# Evaluation Metrics

<!-- This task is evaluated as a classification task. We will use the F1-macro measure for the ranking of teams. There is a limit of 5 runs (total and not per day), and only one person from a team is allowed to submit runs. -->

<!-- Submission Link: Coming Soon -->

<!-- Evaluation File task3/evaluation/CLEF_-_CheckThat__Task3ab_-_Evaluation.txt -->

# Baselines

### Task 5: Authority Finding in Twitter

<!-- For this task, we have created a baseline system. The baseline system can be found at https://zenodo.org/record/6362498 -->

## Scorers


## Credits
Please find it on the task website: https://checkthat.gitlab.io/clef2023/task1/
