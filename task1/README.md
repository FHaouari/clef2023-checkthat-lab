# Task 1: Check-Worthiness in Multimodal and Unimodal Content

The aim of this task is to determine whether a claim in a tweet is worth fact-checking. Typical approaches to make that decision require to either resort to the judgments of professional fact-checkers or to human annotators to answer several auxiliary questions such as "does it contain a verifiable factual claim?", and "is it harmful?", before deciding on the final check-worthiness label.

This year we offer two kinds of data, which translate to two subtasks: 
- **Subtask 1A (Multimodal):** The tweets to be judged include both a text snippet and an image.
- **Subtask 1B (Unimodal - Text):** The tweets to be judged contain only text. 

Subtask 1A is offered in Arabic and English, whereas Subtask 1B is offered in Arabic, English and Spanish.


__Table of contents:__
<!-- - [Evaluation Results](#evaluation-results) -->
- [List of Versions](#list-of-versions)
- [Contents of the Directory](#contents-of-the-repository)
- [File Format](#file-format)
	- [Subtask 1A (Multimodal): Check-Worthiness of multimodal content](#subtask-1a-check-worthiness-of-tweets)
		- [Input Data Format](#input-data-format-1a)
		- [Output Data Format](#output-data-format-1a)
	- [Subtask 1B (Unimodal - Text): Check-Worthiness of unimodal content](#subtask-1a-check-worthiness-of-tweets)
		- [Input Data Format](#input-data-format-1a)
		- [Output Data Format](#output-data-format-1a)
- [Format Checkers](#format-checkers)
- [Scorers](#scorers)
- [Evaluation Metrics](#evaluation-metrics)
- [Baselines](#baselines)
<!-- - [Submission guidelines](#submission-guidelines) -->
- [Credits](#credits)


 
<!-- ## Evaluation Results
Submitted results will be available after the system submission deadline.
Kindly find the leaderboard released in this google sheet, [link](http://shorturl.at/nuCOS). you can find in the tab labeled "Task 1".

**Submission Guidelines:**
- Make sure that you create one account for each team, and submit it through one account only.
- <span style="color:blue;font-size:1.2em;">We will keep the leaderboard private till the end of the submission period, hence, results will not be available upon submission. All results will be available after the evaluation period.</span>
- You are allowed to submit max 200 submissions per day for each subtask.
- The last file submitted to the leaderboard will be considered as the final submission.
- Name of the output file have to be "subtask1[A/B/C/D]_SHORT-NAME-OF-THE-SUBTASK_LANG.tsv" with ".tsv" extension (e.g., subtask1B_claim_arabic.tsv); otherwise, you will get an error on the leaderboard. Subtask are 1A, 1B, 1C, 1D and short name of the subtasks are checkworthy, claim, harmful, and attentionworthy. For task 1, there are six languages (Arabic, Buglarian, Durch, English, Spanish and Turkish).
- You have to zip the tsv, "zip subtask1B_claim_arabic.zip subtask1B_claim_arabic.tsv" and submit it through the codalab page.

**Please submit your results on test data here: https://codalab.lisn.upsaclay.fr/competitions/4230.** -->


## List of Versions
* __subtask-1B [2022/11/23]__
  - Training/Dev/Dev_Test data for subtasks 1B released for Arabic and Spanish.


## Contents of the Directory
<!-- In each directory, we provide subtask-specific zip files. Each zip file contains train, dev, and dev_test data released with the tweets and the labels assigned. We provide a single JSON file for the majority of the languages. The tweet id can be used to match the data between the TSV file and the JSON files.

**Notice** Many instances in the TSV file might not have a corresponding entry in the JSON file. This is due to the deletion of tweets during the compilation of the datasets.
 -->
* Main folder: [data](./data)
  * Subfolder: [subtask-1A-english](./data/subtask-1A-english)
  	This directory contains files for multimodal subtask for the English language.
  * Subfolder: [subtask-1B-arabic](./data/subtask-1B-arabic)
  	This directory contains files for unimodal subtask for the Arabic language.
  * Subfolder: [subtask-1B-spanish](./data/subtask-1B-spanish)
  	This directory contains files for unimodal subtask for the Spanish language.

* Main folder: [baselines](./baselines)<br/>
	Contains scripts provided for baseline models of the tasks
* Main folder: [formet_checker](./format_checker)<br/>
	Contains scripts provided to check format of the submission file
* Main folder: [scorer](./scorer)<br/>
	Contains scripts provided to score output of the model when provided with label (i.e., dev set).

* [README.md](./README.md) <br/>
	This file!


## File Format

### Subtask 1A (Multimodal): Check-Worthiness of multimodal content

#### Input Data Format

#### Output Data Format


### Subtask 1B (Unimodal - Text): Check-Worthiness of unimodal content

#### Input Data Format
For all languages (**Arabic** and **Spanish**) we use the same data format in the train, dev and dev_test files. Each file is TAB seperated (TSV file) containing the tweets and their labels. The text encoding is UTF-8. Each row in the file has the following format:

> tweet_id <TAB> tweet_url <TAB> tweet_text <TAB> class_label

Where: <br>
* tweet_id: Tweet ID for a given tweets given by Twitter <br/>
* tweet_url: URL to the given tweet <br/>
* tweet_text: content of the tweet <br/>
* class_label:
 * *yes* and *no*
 

**Examples:**
> covid-19	1235648554338791427	https://twitter.com/A6Asap/status/1235648554338791427	COVID-19 health advice⚠️ https://t.co/XsSAo52Smu	0<br/>
> covid-19	1235287380292235264	https://twitter.com/ItsCeliaAu/status/1235287380292235264	There's not a single confirmed case of an Asian infected in NYC. Stop discriminating cause the virus definitely doesn't. #racist #coronavirus https://t.co/Wt1NPOuQdy	1<br/>
> covid-19	1236020820947931136	https://twitter.com/ddale8/status/1236020820947931136	Epidemiologist Marc Lipsitch, director of Harvard's Center for Communicable Disease Dynamics: “In the US it is the opposite of contained.' https://t.co/IPAPagz4Vs	1<br/>
> ... <br/>

Note that the gold labels for the task are the ones in the *class_label* column


#### Output Data Format
For all languages (**Arabic** and **Spanish**) the data format is the same, which includes submission files.

For each subtask, the expected results file is a list of tweets with the class label for the particular subtask. Each row contains four TAB separated fields:

> tweet_id <TAB> class_label <TAB> run_id

Where: <br>
* tweet_id: Tweet ID for a given tweets given by Twitter given in the test dataset file<br/>
* class_label: class label for the particular subtask <br/>
* run_id: string identifier used by participants. <br/>

Example:
> covid-19	1235648554338791427	0  Model_1<br/>
> covid-19	1235287380292235264	1  Model_1<br/>
> covid-19	1236020820947931136	0  Model_1<br/>
> ... <br/>



## Format Checkers
The checker for the subtask is located in the [format_checker](./format_checker) module of the project.
To launch the baseline script you need to install packages dependencies found in [requirement.txt](./requirement.txt) using the following:
> pip3 install -r requirements.txt <br/>

The format checker verifies that your generated results files complies with the expected format.
To launch it run:
> python3 format_checker/main.py --subtask=<name_of_the_subtask> --pred-files-path=<path_to_result_file_1 path_to_result_file_2 ... path_to_result_file_n> <br/>

or
> python3 format_checker/subtask_1.py --pred-files-path=<path_to_result_file_1 path_to_result_file_2 ... path_to_result_file_n> <br/>

`--pred-files-path` take a single string that contains a space separated list of file paths. The lists may be of arbitrary positive length (so even a single file path is OK) but their lengths must match.

__<path_to_result_file_n>__ is the path to the corresponding file with participants' predictions, which must follow the format, described in the [Output Data Format](#subtask-1a-check-worthiness-of-debatesspeeches-1) section.

Note that the checker can not verify whether the prediction files you submit contain all lines / claims), because it does not have access to the corresponding gold file.


## Scorers
The scorer for the subtask is located in the [scorer](./scorer) module of the project.
To launch the script you need to install packages dependencies found in [requirement.txt](./requirement.txt) using the following:
> pip3 install -r requirements.txt <br/>

Launch the scorer for the subtask as follows:
> python3 scorer/subtask_1.py --gold-file-path=<path_gold_file> --pred-file-path=<predictions_file> --subtask=<name_of_the_subtask><br/>

The scorer invokes the format checker for the subtask to verify the output is properly shaped.
It also handles checking if the provided predictions file contains all lines/tweets from the gold one.


## Baselines

The [baselines](./baselines) module contains a majority, random and a simple ngram baseline for each subtask.
To launch the baseline script you need to install packages dependencies found in [requirement.txt](./requirement.txt) using the following:
> pip3 install -r requirements.txt <br/>

To launch the baseline script run the following:
> python3 baselines/subtask_1.py --train-file-path=<path_to_your_training_data> --test-file-path=<path_to_your_test_data_to_be_evaluated> --subtask=<name_of_the_subtask> --lang=<language_of_the_subtask><br/>
```
python3 baselines/subtask_1.py --train-file-path=data/subtask-1B-arabic/CT23_1B_checkworthy_arabic_train.tsv --test-file-path=data/subtask-1B-arabic/CT23_1B_checkworthy_arabic_dev.tsv -l arabic -a claim
```
All baselines will be trained on the training tweets and the performance of the model was evaluated on the test tweets.

<!-- ## Submission guidelines
Please follow the submission guidelines discussed here: https://sites.google.com/view/clef2022-checkthat/tasks/task-1-identifying-relevant-claims-in-tweets?#h.sn4sm5zguq98.
 -->
## Credits
Please find it on the task website: https://checkthat.gitlab.io/clef2023/task1/

<!-- Contact:   clef-factcheck@googlegroups.com -->
